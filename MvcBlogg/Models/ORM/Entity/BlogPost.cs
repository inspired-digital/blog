﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcBlogg.Models.ORM.Entity
{
    public class BlogPost : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }


        public int CategoryId { get; set; }

        public string ImagePath { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public virtual List<BlogPostComment> BlogPostComment { get; set; }

    }
}