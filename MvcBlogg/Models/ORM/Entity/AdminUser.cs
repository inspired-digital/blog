﻿using System.ComponentModel.DataAnnotations;

namespace MvcBlogg.Models.ORM.Entity
{
    public class AdminUser : BaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        [Required]
        [StringLength(30)]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }



    }
}