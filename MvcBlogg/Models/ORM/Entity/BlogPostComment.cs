﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MvcBlogg.Models.ORM.Entity
{
    public class BlogPostComment : BaseEntity
    {
        public int BlogPostId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        //public string Title { get; set; }
        public string Content { get; set; }

        private bool _isActive = false;
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }


        [ForeignKey("BlogPostId")]
        public virtual BlogPost BlogPost { get; set; }
    }
}