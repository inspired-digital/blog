﻿using System.ComponentModel.DataAnnotations;

namespace MvcBlogg.Models.ORM.Entity
{
    public class Category : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}