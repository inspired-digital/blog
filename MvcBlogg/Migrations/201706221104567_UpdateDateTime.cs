namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDateTime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AdminUser", "AddDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AdminUser", "AddDate", c => c.DateTime(nullable: false));
        }
    }
}
