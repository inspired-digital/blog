namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NameSurnameAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdminUser", "Name", c => c.String());
            AddColumn("dbo.AdminUser", "Surname", c => c.String());
            AlterColumn("dbo.AdminUser", "Email", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AdminUser", "Email", c => c.String(nullable: false));
            DropColumn("dbo.AdminUser", "Surname");
            DropColumn("dbo.AdminUser", "Name");
        }
    }
}
