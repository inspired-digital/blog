namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedBlogComentTableV4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BlogPostComment", "Title");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BlogPostComment", "Title", c => c.String());
        }
    }
}
