namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBlogpostComments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlogPostComment",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        BlogPostId = c.Int(nullable: false),
                        AddDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleteDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BlogPost", t => t.BlogPostId, cascadeDelete: true)
                .Index(t => t.BlogPostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlogPostComment", "BlogPostId", "dbo.BlogPost");
            DropIndex("dbo.BlogPostComment", new[] { "BlogPostId" });
            DropTable("dbo.BlogPostComment");
        }
    }
}
