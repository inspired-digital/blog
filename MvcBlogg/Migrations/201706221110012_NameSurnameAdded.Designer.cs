// <auto-generated />
namespace MvcBlogg.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class NameSurnameAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(NameSurnameAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201706221110012_NameSurnameAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
