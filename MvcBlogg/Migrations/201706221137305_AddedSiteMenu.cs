namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSiteMenu : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SiteMenu",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                        cssClass = c.String(),
                        AddDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleteDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SiteMenu");
        }
    }
}
