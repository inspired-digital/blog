namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedBlogComentTableV3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogPostComment", "Email", c => c.String());
            DropColumn("dbo.BlogPostComment", "Surname");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BlogPostComment", "Surname", c => c.String());
            DropColumn("dbo.BlogPostComment", "Email");
        }
    }
}
