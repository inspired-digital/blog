namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequiredField : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AdminUser", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.AdminUser", "Password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AdminUser", "Password", c => c.String());
            AlterColumn("dbo.AdminUser", "Email", c => c.String());
        }
    }
}
