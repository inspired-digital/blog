namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedBlogComentTableV2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogPostComment", "Name", c => c.String());
            AddColumn("dbo.BlogPostComment", "Surname", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlogPostComment", "Surname");
            DropColumn("dbo.BlogPostComment", "Name");
        }
    }
}
