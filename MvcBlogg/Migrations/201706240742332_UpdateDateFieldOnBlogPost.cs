namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDateFieldOnBlogPost : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AdminUser", "DeleteDate", c => c.DateTime());
            AlterColumn("dbo.BlogPost", "DeleteDate", c => c.DateTime());
            AlterColumn("dbo.Category", "DeleteDate", c => c.DateTime());
            AlterColumn("dbo.SiteMenu", "DeleteDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SiteMenu", "DeleteDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Category", "DeleteDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.BlogPost", "DeleteDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AdminUser", "DeleteDate", c => c.DateTime(nullable: false));
        }
    }
}
