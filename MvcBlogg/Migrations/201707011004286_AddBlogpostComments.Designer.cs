// <auto-generated />
namespace MvcBlogg.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddBlogpostComments : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddBlogpostComments));
        
        string IMigrationMetadata.Id
        {
            get { return "201707011004286_AddBlogpostComments"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
