namespace MvcBlogg.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedImagePathtoBlog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogPost", "ImagePath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlogPost", "ImagePath");
        }
    }
}
