﻿using MvcBlogg.Areas.Admin.Models.DTO;
using MvcBlogg.Models.ORM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MvcBlogg.Areas.Admin.Controllers
{
    public class CategoryController : BaseController
    {
        public ActionResult Index()
        {
            List<CategoryVm> model = db.Categories.Where(x => x.IsDeleted == false)
                .OrderBy(x => x.AddDate).Select(x => new CategoryVm()
                {
                    Name = x.Name,
                    Description = x.Description,
                    ID = x.ID
                }).ToList();

            return View(model);
        }

        public ActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCategory(CategoryVm model)
        {
            if (ModelState.IsValid)
            {
                Category category = new Category();
                category.Name = model.Name;
                category.Description = model.Description;

                db.Categories.Add(category);
                db.SaveChanges();
                ViewBag.Isuccess = 1;
                return View();
            }
            else
            {
                ViewBag.Isuccess = 2;
                return View();
            }
        }

        public JsonResult DeleteCategory(int id)
        {
            Category category = db.Categories.FirstOrDefault(x => x.ID == id);
            category.IsDeleted = true;
            category.DeleteDate = DateTime.Now;
            db.SaveChanges();

            return Json("");
        }

        public ActionResult UpdateCategory(int id)
        {
            Category category = db.Categories.FirstOrDefault(x => x.ID == id);
            CategoryVm model = new CategoryVm();
            model.Name = category.Name;
            model.Description = category.Description;

            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateCategory(CategoryVm model)
        {
            if (ModelState.IsValid)
            {
                Category category = db.Categories.FirstOrDefault(x => x.ID == model.ID);
                category.Name = model.Name;
                category.Description = model.Description;
                db.SaveChanges();
                ViewBag.IsSuccess = 1;
                return RedirectToAction("Index", "Category");
            }
            else
            {
                ViewBag.IsSuccess = 1;
                return View(model);
            }
        }
    }
}