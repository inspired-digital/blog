﻿using MvcBlogg.Models.ORM.Context;
using System.Web.Mvc;

namespace MvcBlogg.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        public BlogContext db;

        public BaseController()
        {
            //ViewBag.User = HttpContext.User.Identity.Name;
            db = new BlogContext();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }
    }
}