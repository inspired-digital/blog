﻿using MvcBlogg.Areas.Admin.Models.DTO;
using MvcBlogg.Areas.Admin.Models.Services;
using MvcBlogg.Models.ORM.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace MvcBlogg.Areas.Admin.Controllers
{
    public class BlogPostController : BaseController
    {
        // GET: Admin/BlogPost
        public ActionResult Index()
        {
            List<BlogPostVm> model = db.BlogPosts.Where(x => x.IsDeleted == false)
                .OrderBy(x => x.AddDate)
                .Select(x => new BlogPostVm()
                {
                    ID = x.ID,
                    Title = x.Title,
                    CategoryName = x.Category.Name
                }).ToList();

            return View(model);
        }

        public ActionResult AddBlogPost()
        {
            BlogPostVm model = new BlogPostVm();
            model.drpCategories = DrpServices.getDrpCategories();

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddBlogPost(BlogPostVm model)
        {
            BlogPostVm vmodel = new BlogPostVm();
            vmodel.drpCategories = DrpServices.getDrpCategories();



            if (ModelState.IsValid)
            {

                string fileName = "";
                foreach (string name in Request.Files)
                {
                    model.PostImage = Request.Files[name];
                    //string imgNumber = Guid.NewGuid().ToString();
                    string ext = Path.GetExtension(model.PostImage.FileName);
                    if (ext==".jgp"||ext==".jpeg"||ext=="png")
                    {
                        fileName = model.PostImage.FileName;
                        model.PostImage.SaveAs(Server.MapPath("~/Uploads/Images/" + fileName));
                    }                    
                }
                BlogPost blogpost = new BlogPost();
                blogpost.CategoryId = model.CategoryId;
                blogpost.Title = model.Title;
                blogpost.Title = model.Title;
                blogpost.Content = model.Content;
                blogpost.ImagePath = fileName;

                db.BlogPosts.Add(blogpost);
                ViewBag.IsSuccess = 1;
                db.SaveChanges();
                return View(vmodel);
            }
            else
            {
                ViewBag.IsSuccess = 2;
                return View(vmodel);
            }
        }

        public ActionResult UpdateBlog(int id)
        {

            BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == id);
            BlogPostVm model = new BlogPostVm();
            model.Title = blogpost.Title;
            model.CategoryId = blogpost.CategoryId;
            model.Content = blogpost.Content;
            model.drpCategories = DrpServices.getDrpCategories();

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateBlog(BlogPostVm model)
        {
            model.drpCategories = DrpServices.getDrpCategories();

            if (ModelState.IsValid)
            {
                BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == model.ID);
                blogpost.CategoryId = model.CategoryId;
                blogpost.Title = model.Title;
                blogpost.Content = model.Content;

                db.SaveChanges();
                ViewBag.IsSuccess = 1;
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.IsSuccess = 2;
                return View(model);
            }
        }

        public JsonResult DeleteBlog(int id)
        {
            BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == id);
            blogpost.IsDeleted = true;
            blogpost.DeleteDate = DateTime.Now;
            db.SaveChanges();

            return Json("");
        }
    }
}