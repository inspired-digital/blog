﻿using MvcBlogg.Areas.Admin.Models.Attributes;
using System.Web.Mvc;

namespace MvcBlogg.Areas.Admin.Controllers
{
    [LoginControl]
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {


            //BlogContext db = new BlogContext();
            //string email = HttpContext.User.Identity.Name;
            //AdminUser adminUser = db.AdminUsers.FirstOrDefault(x => x.Email == email);
            //string name = adminUser.Name;
            //string surname = adminUser.Surname;

            return View();
        }
    }
}