﻿using MvcBlogg.Areas.Admin.Models.DTO;
using MvcBlogg.Models.ORM.Entity;
using System.Web.Mvc;

namespace MvcBlogg.Areas.Admin.Controllers
{
    public class SiteMenuController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddSiteMenu(SiteMenuVm model)
        {
            if (ModelState.IsValid)
            {
                SiteMenu sitemenu = new SiteMenu();
                sitemenu.Name = model.Name;
                sitemenu.Url = model.Url;
                sitemenu.cssClass = model.cssClass;

                db.SiteMenus.Add(sitemenu);
                db.SaveChanges();
                ViewBag.IsSuccess = 1;
                return View();
            }
            else
            {
                ViewBag.IsSuccess = 2;
                return View(model);
            }
        }
    }
}