﻿using System.Web;
using System.Web.Mvc;

namespace MvcBlogg.Areas.Admin.Models.Attributes
{
    public class LoginControl : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.HttpContext.Response.Redirect("/Admin/login/Index");
            }
        }
    }
}