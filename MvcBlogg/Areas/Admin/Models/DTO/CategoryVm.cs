﻿using System.ComponentModel.DataAnnotations;

namespace MvcBlogg.Areas.Admin.Models.DTO
{
    public class CategoryVm : BaseVm
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        public string Description { get; set; }


    }
}