﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace MvcBlogg.Areas.Admin.Models.DTO
{
    public class BlogPostVm : BaseVm
    {
        public string Title { get; set; }
        public string Content { get; set; }

        public string CategoryName { get; set; }

        [Display(Name = "Post Image")]
        public HttpPostedFileBase PostImage { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public IEnumerable<SelectListItem> drpCategories { get; set; }

    }
}