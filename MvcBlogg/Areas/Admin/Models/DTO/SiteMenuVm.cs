﻿using System.ComponentModel.DataAnnotations;

namespace MvcBlogg.Areas.Admin.Models.DTO
{
    public class SiteMenuVm : BaseVm
    {
        [Required]
        public string Name { get; set; }

        public string Url { get; set; }

        public string cssClass { get; set; }
    }
}