﻿using System.ComponentModel.DataAnnotations;

namespace MvcBlogg.Areas.Admin.Models.DTO
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Invalid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Invalid Password")]
        public string Password { get; set; }
    }
}