﻿using MvcBlogg.Models.ORM.Entity;
using System.Collections.Generic;

namespace MvcBlogg.Areas.Admin.Models.DTO
{
    public class MultiModelTest
    {
        public List<Category> Categories { get; set; }

        public List<AdminUser> AdminUsers { get; set; }
    }
}