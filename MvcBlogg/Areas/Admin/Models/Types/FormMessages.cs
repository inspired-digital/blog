﻿namespace MvcBlogg.Areas.Admin.Models.Types
{
    public class FormMessages
    {
        public static string addSucess = "Record successfully added";
        public static string addError = "Attemp to save record failed";
    }
}