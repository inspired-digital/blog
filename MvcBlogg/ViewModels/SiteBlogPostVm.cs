﻿namespace MvcBlogg.ViewModels
{
    public class SiteBlogPostVm
    {
        public int BlogPostId { get; set; }

        public string Title { get; set; }

        public string PostImagePath { get; set; }

        public string Content { get; set; }

        public string Category { get; set; }
    }
}