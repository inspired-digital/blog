﻿using System.ComponentModel.DataAnnotations;

namespace MvcBlogg.ViewModels
{
    public class CommentVm
    {
        public int BlogPostId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        //[Display(Name = "Title")]
        //public string Title { get; set; }

        [Display(Name = "Comment")]
        public string Content { get; set; }
    }
}