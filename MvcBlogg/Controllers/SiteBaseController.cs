﻿using MvcBlogg.Models.ORM.Context;
using System.Web.Mvc;

namespace MvcBlogg.Controllers
{
    public class SiteBaseController : Controller
    {
        public BlogContext db;
        public SiteBaseController()
        {
            db = new BlogContext();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }
    }
}