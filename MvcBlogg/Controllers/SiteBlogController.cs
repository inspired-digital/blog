﻿using MvcBlogg.Models.ORM.Entity;
using MvcBlogg.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace MvcBlogg.Controllers
{
    public class SiteBlogController : SiteBaseController
    {
        // GET: SiteBlog
        public ActionResult Index(int id)
        {
            BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == id);
            SiteBlogPostVm model = new SiteBlogPostVm();
            model.Content = blogpost.Content;
            model.PostImagePath = blogpost.ImagePath;
            model.Title = blogpost.Title;
            model.Category = blogpost.Category.Name;
            model.BlogPostId = blogpost.ID;

            return View(model);
        }

        [HttpPost]
        public ActionResult AddComment(CommentVm model)
        {
            BlogPostComment comment = new BlogPostComment();

            //comment.Title = model.Title;
            comment.Content = model.Content;
            comment.ID = model.BlogPostId;
            comment.Name = model.Name;
            comment.Email = model.Email;

            db.BlogPostComments.Add(comment);
            db.SaveChanges();

            return RedirectToAction("Index", "SiteBlog", new { id = model.BlogPostId });
        }
    }
}